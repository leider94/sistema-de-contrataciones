<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContratacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {   
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });

        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username',64)->unique();
            $table->string('description',128)->nullable(false);
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->integer('area_id')->unsigned();
          //  $table->rememberToken();
            $table->foreign('area_id')->references('id')->on('areas')->onDelete('cascade');            
            $table->timestamps();
        });

        Schema::create('tipo_contratacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });

        Schema::create('subtipo_contratacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre',128)->nullable(false);
            $table->integer('tipocontratacion_id')->unsigned()->nullable();

            $table->foreign('tipocontratacion_id')->references('id')
                    ->on('tipo_contratacion')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('partidas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('codigo');
            $table->string('nombre',128)->nullable(false);
            $table->timestamps();
        });


        Schema::create('solicitud', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('objeto',128)->nullable(false);
            $table->string('objetivo',128)->nullable(false);
            $table->string('antecedente')->nullable(false);
            $table->string('justificacion_tecnica')->nullable(false);

            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('tipocontratacion_id')->unsigned()->nullable();
            $table->integer('subtipocontratacion_id')->unsigned()->nullable(true);

            $table->foreign('usuario_id')->references('id')
                    ->on('users')->onDelete('cascade');

            $table->foreign('tipocontratacion_id')->references('id')
                    ->on('tipo_contratacion')->onDelete('cascade');

            $table->foreign('subtipocontratacion_id')->references('id')
                    ->on('subtipo_contratacion')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('partida_solicitud', function (Blueprint $table) {
            $table->integer('solicitud_id')->unsigned()->nullable();
            $table->foreign('solicitud_id')->references('id')
                    ->on('solicitud')->onDelete('cascade');

            $table->integer('partida_id')->unsigned()->nullable();
            $table->foreign('partida_id')->references('id')
                    ->on('partidas')->onDelete('cascade');
            $table->timestamps();
        });




        Schema::create('proveedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->string('direccion',128)->nullable(false);
            $table->timestamps();
        });

        Schema::create('tipo_referencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->timestamps();
        });

        Schema::create('referencias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->integer('tiporeferencia_id')->unsigned();
            
            $table->foreign('tiporeferencia_id')->references('id')
                ->on('tipo_referencias')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('referencia_proveedor', function (Blueprint $table) {
            $table->integer('proveedor_id')->unsigned()->nullable();
            $table->foreign('proveedor_id')->references('id')
                    ->on('proveedores')->onDelete('cascade');

            $table->integer('referencia_id')->unsigned()->nullable();
            $table->foreign('referencia_id')->references('id')
                    ->on('referencias')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('tipo_materiales', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('descripcion',128)->nullable(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('tipo_contratacion');
        Schema::dropIfExists('subtipo_contratacion');
        Schema::dropIfExists('partidas');
        Schema::dropIfExists('solicitud');
        Schema::dropIfExists('solicitud_partida');
        Schema::dropIfExists('tipo_materiales');
        Schema::dropIfExists('referencia_proveedor');
        Schema::dropIfExists('proveedores');
        Schema::dropIfExists('referencias');
        Schema::dropIfExists('tipo_referencias');
        Schema::dropIfExists('users');
        Schema::dropIfExists('areas');
        
       
    }
}
