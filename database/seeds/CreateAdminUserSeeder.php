<?php

use Illuminate\Database\Seeder;
use App\User;
use App\TypeMaterial;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        if(DB::table('areas')->get()->count() == 0)
			{
				DB::table('areas')->insert([
          [
						'nombre' => 'Área - Dirección General Ejecutiva',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Transparencia y Lucha Contra La Corrupción',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Auditoria Interna',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Asesoría Legal',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Sistemas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Gestión Institucional y Desarrollo Organizacional	',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Área - Coordinación de Planificación y Evaluación de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Planificación y Evaluación de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Control, Seguimiento y Monitorio de Proyectos',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Área - Coordinación Administrativa Financiera',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Administración',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'nombre' => 'Unidad - Finanzas',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]      
        ]);
      } 
      else 
      {
        	echo "\e[31mTable areas is not empty, therefore NOT "; 
      }




      $user = User::create([
        'username' => 'miguel.sanchez', 
        'description' => 'Pasante - Unidad Sistemas', 
      'name' => 'Miguel Sanchez', 
      'email' => 'miguels@gmail.com',
      'password' => bcrypt('123456'),
      'area_id' => '5'
    ]);

    $role = Role::create(['name' => 'Admin']);

    $permissions = Permission::pluck('id','id')->all();

    $role->syncPermissions($permissions);

    $user->assignRole([$role->id]);


        if(DB::table('tipo_materiales')->get()->count() == 0)
			{
				DB::table('tipo_materiales')->insert([
          [
						'descripcion' => 'Equipos de Computadoras',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
						'descripcion' => 'Muebles',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]       
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_materiales is not empty, therefore NOT "; 
      }

      if(DB::table('tipo_contratacion')->get()->count() == 0)
			{
				DB::table('tipo_contratacion')->insert([
          [
						'nombre' => 'Contratación menor',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'nombre' => 'Contratación Directa',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

						'nombre' => 'Contratación Ampe',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_contratacion is not empty, therefore NOT "; 
      }


      if(DB::table('subtipo_contratacion')->get()->count() == 0)
			{
				DB::table('subtipo_contratacion')->insert([
          [
            'nombre' => 'Bienes',
            'tipocontratacion_id' => '1',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

            'nombre' => 'Servicios Generales',
            'tipocontratacion_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [

            'nombre' => 'Consultoria',
            'tipocontratacion_id' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable subtipo_contratacion is not empty, therefore NOT "; 
      }



        if(DB::table('tipo_referencias')->get()->count() == 0)
			{
				DB::table('tipo_referencias')->insert([
          [
          	'id' => '2',
						'descripcion' => 'Email',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
          	'id' => '3',
						'descripcion' => 'Celular',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
          	'id' => '4',
						'descripcion' => 'Tefefono',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable tipo_referencias is not empty, therefore NOT "; 
      }

      if(DB::table('partidas')->get()->count() == 0)
			{
				DB::table('partidas')->insert([
          [
            'codigo' => '43110',
						'nombre' => 'Equipo de Oficina y Muebles',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'codigo' => '43120',
						'nombre' => 'Equipo de Computación',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ],
          [
            'codigo' => '43500',
						'nombre' => 'Equipo de Comunicacion',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
          ]              
        ]);
      } 
      else 
      {
        	echo "\e[31mTable partidas is not empty, therefore NOT "; 
      }


    }

    
}
