<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::group(['middleware' => ['auth']], function() {


    Route::group(['middleware' => ['role:Admin']], function () {

        Route::resource('roles','RoleController');
        Route::resource('users','UserController');
        Route::resource('products','ProductController');


        //Route::resource('administrator/supplier', 'AdministratorController\SupplierController',['names' => [
		//	'store' => 'administrator.supplier.store',
		//	'index' => 'administrator.supplier.index',
		//	'create' => 'administrator.supplier.create',
		//	'show' => 'administrator.supplier.show',
		//	'edit' => 'administrator.supplier.edit',
		//]]);
		//Route::post('administrator/supplier/update', 'AdministratorController\SupplierController@update')->name('administrator.supplier.update');
		//Route::get('administrator/supplier/destroy/{id}', 'AdministratorController\SupplierController@destroy');
		
		Route::get('solicitud', 'SolicitudController@index')->name('solicitud');
		Route::get('solicitud/partida', 'SolicitudController@partidas')->name('solicitud.partidas');
		Route::get('solicitud/word', 'SolicitudController@generadorWord')->name('solicitud.word');
		Route::get('solicitud/{id}', 'SolicitudController@porTipo');
		
		Route::post('solicitud/agregarSolicitud','SolicitudController@añadirSolicitud');
		//Route::resource('notasinternas','NotaInternaController',['names' => [
		//	'store' => 'notainterna.store',
		//	'index' => 'notainterna.index',
		//	'show' => 'notainterna.show',
		//]]);


		Route::resource('administrator/supplier','AdministratorController\SupplierController',['names' => [
			'store' => 'administrator.supplier.store',
			'index' => 'administrator.supplier.index',
			'show' => 'administrator.supplier.show',
		]]);

    	Route::post('administrator/supplier/addSupplier','AdministratorController\SupplierController@addSupplier');
    	Route::post('administrator/supplier/editSupplier','AdministratorController\SupplierController@editSupplier');
		Route::post('administrator/supplier/deleteSupplier','AdministratorController\SupplierController@deleteSupplier');

		

		
		Route::resource('administrator/typematerial','AdministratorController\TypeMaterialsController',['names' => [
			'store' => 'administrator.typematerial.store',
			'index' => 'administrator.typematerial.index',
			'show' => 'administrator.typematerial.show',
		]]);

    	Route::post('administrator/typematerial/addTypeMaterial','AdministratorController\TypeMaterialsController@addTypeMaterial');
    	Route::post('administrator/typematerial/editTypeMaterial','AdministratorController\TypeMaterialsController@editTypeMaterial');
		Route::post('administrator/typematerial/deleteTypeMaterial','AdministratorController\TypeMaterialsController@deleteTypeMaterial');
		
		//Route::resource('post','PostController');
    	//Route::POST('addPost','PostController@addPost');
    	//Route::POST('editPost','PostController@editPost');
    	//Route::POST('deletePost','PostController@deletePost');
    


	});

    
});
