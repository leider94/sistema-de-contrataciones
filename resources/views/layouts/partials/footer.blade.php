<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Unidad de Tecnologías de Información y Comunicación &copy; 2019 <a href="https://www.fonabosque.gob.bo/" target="_blank">FONABOSQUE</a>.</strong> Fondo Nacional de Desarrollo Forestal.
  </footer>