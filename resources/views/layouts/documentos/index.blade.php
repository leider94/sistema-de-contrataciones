@extends('layouts.partials.layout')
@section('content')
<div class="col-xs-12">
  <div class="box box-info">
      <div class="box-header">
          <h3 class="box-title">Solicitud de Contrataciones</h3>
      </div>
      <div class="box-body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped" id="table">
              <thead>
                <tr>
                  <th width="150px">No</th>
                  <th>Objeto de Contratación</th>
                  <th>Objetivo</th>
                  <th>Estado</th>
                  <th class="text-center" width="150px">
                    <a href="#" class="create-modal btn btn-success btn-sm">
                      <i class="glyphicon glyphicon-plus"></i>
                    </a>
                </th>
              </thead>
            </tr>
            {{ csrf_field() }}
            <?php  $no=1; ?>
            @foreach ($solicitud as $value)
              <tr>
                <td>{{ $no++ }}</td>
                <td>{{ $value->objeto }}</td>
                <td>{{ $value->objetivo }}</td>
                <td><span class="label bg-yellow">Pendiente</span></td>
                
                  
         
                  
                <td>
                  <a href="#" class="show-modal btn btn-info btn-sm">
                    <i class="fa fa-eye"></i>
                  </a>
                  <a href="#" class="edit-modal btn btn-warning btn-sm">
                    <i class="glyphicon glyphicon-pencil"></i>
                  </a>
                  <a href="#" class="delete-modal btn btn-danger btn-sm">
                    <i class="glyphicon glyphicon-trash"></i>
                  </a>
                  <a href="{{ url('solicitud/word') }}" class="download-modal btn btn-info btn-sm">
                    <i class="glyphicon glyphicon-download-alt"></i>
                  </a>
                </td>
              </tr>
              @endforeach
          </table>
        </div>
        {{$solicitud->links()}}
      </div>
  </div>  
</div>
{{-- Modal Form Create Supplier --}}
<div id="create" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal request-form" role="form" >
          <div class="form-group">  
            <div class="col-sm-6">
                <label>Tipo de Contratacion</label>
                <select name="type1" id="type1" class="form-control">
                  <option disabled selected>Seleccione</option>
            
                  @foreach( $tipocontratacion as $key => $value )
                  <option value="{{ $key }}">{{ $value }}</option>
                  @endforeach
              
                </select>
            </div>
            <div class="col-sm-6">
                <label>Subtipo</label>
                <select name="type2" id="type2" class="form-control">
                  <option disabled selected>Seleccione</option> 
              
                </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-sm-12">
              <label for="title">Objeto de la Contratación</label>
              <input type="text" class="form-control" id="objeto" name="objeto"
              placeholder="Ingrese el objeto" required>
              <p class="error text-center alert alert-danger hidden"></p>
            </div>
          </div>

          <div class="form-group">
              <div class="col-sm-12">
                <label for="title">Objetivo de la Contratación</label>
                <input type="text" class="form-control" id="objetivo" name="objetivo"
                placeholder="Ingrese el objeto" required>
                <p class="error text-center alert alert-danger hidden"></p>
              </div>
            </div>

          <div class="form-group shadow-textarea">
              <div class="col-sm-12">
                <label for="title">Antecedentes</label>
                <textarea class="form-control z-depth-1" id="antecedentes" name="antecedentes" rows="3" placeholder="Escribe algo aquí..."></textarea>
                <p class="error text-center alert alert-danger hidden"></p>
              </div>
          </div>

          <div class="form-group shadow-textarea">
              <div class="col-sm-12">
                <label for="title">Justificación Técnica</label>
                <textarea class="form-control z-depth-1" id="justificacion" name="justificacion" rows="3" placeholder="Escribe algo aquí..."></textarea>
                <p class="error text-center alert alert-danger hidden"></p>
              </div>
          </div>

          <div class="form-group">  
              <div class="col-sm-6">
                    <label for="title">Partidas</label>
                    <input type="text" name="buscar" id="buscar" class="form-control" placeholder="Busque por codigo ó descripción" />
              </div>
          </div>

          <div class="form-group">  
            <div class="col-sm-12">
              <div class="table-responsive">
                <table class="table table-striped table-bordered">
                 <thead>
                  <tr>
                   <th>Codigo</th>
                   <th>Descripción</th>
                   <th>Seleccionados</th>
                  </tr>
                 </thead>
                 <tbody name="partidas" id="partidas">
          
                 </tbody>
                </table>
               </div>
            </div>
        </div>
            
          
          

        </form>
      </div>
          <div class="modal-footer">
            <button class="btn btn-warning" type="submit" id="add">
              <span class="glyphicon glyphicon-plus"></span>Save Post
            </button>
            <button class="btn btn-warning" type="button" data-dismiss="modal">
              <span class="glyphicon glyphicon-remobe"></span>Close
            </button>
          </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    {{-- ajax Form Add Type Supplier--}}
  $(document).ready( function () {  
 // var checks =  new Array();
  var array_check=[];
  //var i=1; 
  //views_name=[{id: 1,select: "sel-1",input: "inp-1"}];
  //Flat red color scheme for iCheck

  

  $('#table').DataTable({
    'paging'      : true,
    'lengthChange': true,
    'searching'   : true,
    'info'        : true,
    'autoWidth'   : false,
    "pageLength": 20,
    "language": {
    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
  }
  });
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
  $(document).on('change','input[type="checkbox"]' ,function(e) {
    if(this.id=="nacional") {
        if(this.checked) {console.log(this.value);//$('#id_nacional').val(this.value);
        array_check.push(this.value) ;
        }else {
          for( var i = 0; i < array_check.length; i++){ 
           // console.log(array_check[i]);
            console.log(this.value);
             if ( array_check[i] === this.value) {
              array_check.splice(i, 1); 
             }
          }
          console.log((this.value)+" des-seleccion");
          
        } 
        console.log("checks");
          console.log(array_check);  
        }
  });

  //$('[name="checks[]"]').click(function() {
  //    var arr = $('[name="checks[]"]:checked').map(function(){
  //      
  //      return this.value;
  //    }).get();
  //    
  //    var str = arr.join(',');
  //    checks=[{id: str}];
  //    console.log(JSON.stringify(arr));
  //    console.log(str);
  //  });

  fetch_customer_data();
  function fetch_customer_data(query = ''){
    $.ajax({
     url:"{{ route('solicitud.partidas') }}",
     method:'get',
     data:{
       query:query,
      arraycheck:JSON.stringify(array_check)},
     dataType:'json',
     success:function(data)
     {
      console.log(data.checks);
      //  var salida="";
      // if (data.length>0) {
      //    for (index=0; index < data.length; index++) {
      //      salida+='<tr>'+
      //               '<td>'+data[index].codigo+'</td>'+
      //               '<td>'+data[index].nombre+'</td>'+
      //               '<td><input id="nacional" name="nacional" type="checkbox" class="flat-red" value="'+data[index].id+'"></td>'+
      //              '</tr>';
      //    }
      // }else{
      //  console.log(data.length);
      //  salida ='<tr>'+
      //              '<td align="center" colspan="5">Partida no encontrada</td>'+
      //            '</tr>';
      // }
      // $('#partidas').html(output);
     
      $('#partidas').html(data.table_data);
     // $('#total_records').text(data.total_data);
     }
    })
  }

   $(document).on('keyup', '#buscar', function(){
    var query = $(this).val();
    console.log(query);
    fetch_customer_data(query);
   });

    
  
  
          //$('#more').click(function(){
          //  i++;
          //  var html='<div class="form-group" id="'+i+'">'+
          //    '<div class="col-sm-4">'+
          //        '<select class="form-control" name="sel-'+i+'">'+
          //        '<option disabled selected>Seleccione</option>';

          //    html+="<option value='"+index+"'>"+value+"</option>";
          //  });
          //  html+='</select>'+
          //      '</div>'+
          //      '<div class="col-sm-6">'+
          //          '<input type="text" class="form-control" name="inp-'+i+'" id="body"'+
          //          'placeholder="Ingrese su referencia" required>'+
          //          '<p class="error text-center alert alert-danger hidden"></p>'+
          //      '</div>'+
          //      '<div class="col-sm-2">'+
          //          '<button class="btn btn-danger remove_button" type="submit" title="Eliminar">'+
          //              '<span class="glyphicon glyphicon-minus"></span>'+
          //            '</button>'+
          //      '</div>'+
          //    '</div>';
          //          $('.supplier-form').append(html);
          //          views_name.push({
          //          id: i,
          //          select: "sel-"+i,
          //          input: "inp-"+i
          //        });
          //        console.log(views_name);
          //   });
        //
  
          //$('.supplier-form').on("click",".remove_button", function(e){
          //  e.preventDefault(); 
          //  $(this).parent().parent('div').remove(); 
          //  console.log(i);
          //  console.log($(this).parent().parent('div').prop("id"));
          //  var id_view=$(this).parent().parent('div').prop("id");
          //  for (index=0; index < views_name.length; index++) {
          //    if(views_name[index].id == id_view ){
          //     console.log(views_name[index].id+" es igual a "+id_view);
          //     views_name.splice(index, 1);
          //     console.log(views_name);
          //    }
          //  }
          //});
          
        // function Add Type Suppliers
    $(document).on('click','.create-modal', function() {
      $('#create').modal('show');
      $('.form-horizontal').show();
      $('.modal-title').text('SOLICITUD DE CONTRATACIÓN');
    });
    $("#add").click(function() {
      console.log("id select typo 1: "+document.getElementsByName("type1")[0].value);
      console.log("id select typo 2: "+document.getElementsByName("type2")[0].value);
      console.log($('input[name=objeto]').val());
      console.log($('input[name=objetivo]').val());
      console.log($('textarea#antecedentes').val());
      console.log($('textarea#justificacion').val());
      //console.log(checks);
      //console.log($('input[name=title]').val());
      //console.log($('input[name=direction]').val());
      //var views_result =  new Array();
      //for (index=0; index < views_name.length; index++){
      //  views_result.push({id_type: document.getElementsByName(views_name[index].select)[0].value,
      //                            reference: $('input[name='+views_name[index].input+']').val()});
      //}
   //   console.log(views_result);
  //
      $.ajax({
        type: 'POST',
        url: '/solicitud/agregarSolicitud',
        data: {
                '_token': $('input[name=_token]').val(),
                'tipo1': document.getElementsByName("type1")[0].value,
                'tipo2': document.getElementsByName("type2")[0].value,
                'objeto': $('input[name=objeto]').val(),
                'objetivo': $('input[name=objetivo]').val(),
                'antecedentes': $('textarea#antecedentes').val(),
                'justificacion': $('textarea#justificacion').val(),
                'ckecks': JSON.stringify(array_check),
                },
        dataType: "json",
        success: function(data){
          console.log("eroor");
           if ((data.errors)) {
             $('.error').removeClass('hidden');
             $('.error').text(data.errors.type1);
             $('.error').text(data.errors.type2);
             $('.error').text(data.errors.objeto);
             $('.error').text(data.errors.objetivo);
             $('.error').text(data.errors.antecedentes);
             $('.error').text(data.errors.justificacion);
           }else{
            var no = '<?php echo $no;?>'
             console.log(data);
             $('.error').remove();
             $('#table').append("<tr class='post" + data.id + "'>"+
              "<td>" + no + "</td>"+
             "<td>" + data.objeto + "</td>"+
             "<td>" + data.objetivo + "</td>"+
             "<td><span class='label bg-yellow'>Pendiente</span></td>"+
             "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='glyphicon glyphicon-trash'></span></button><button class='download-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.objeto + "'><span class='glyphicon glyphicon-save'></span></button></td>"+
             "</tr>");
        }
       },
      });       
      $('#objeto').val('');
      $('#objetivo').val('');
      $('#antecedentes').val('');
      $('#justificacion').val('');
      $('#buscar').val('');
      array_check=[];
      fetch_customer_data('');
      });

      $("#type1").change(function(){
        var categoria = $(this).val();
        console.log(categoria);
        disabledtipo2(categoria);
        $.get('/solicitud/'+categoria, function(data){
        //esta el la peticion get, la cual se divide en tres partes. ruta,variables y funcion
          console.log(data);
          
            var producto_select = '<option disabled selected>Seleccione subcategoria</option>'
              for (var i=0; i<data.length;i++)
                producto_select+='<option value="'+data[i].id+'">'+data[i].nombre+'</option>';

              $("#type2").html(producto_select);

        });
      });

      
      
    
    });

  function disabledtipo2(sel){
    var sel=parseInt(sel);
    switch (sel) {
      case 1:
        document.getElementById("type2").disabled=false;
        console.log("valor : "+sel);
        break;
      case 2:
        document.getElementById("type2").disabled=true;
        console.log("valor : "+sel);
        break;
      case 3:
        document.getElementById("type2").disabled=true;
        break;
      default:
       console.log("selector no esperado : "+sel);
        break;
    }
      //sel=parseInt(sel);
      
  }
  
  
  
  // function Edit Type Materials
 // $(document).on('click', '.edit-modal', function() {
 //     $('#footer_action_button').text(" Update Post");
 //     $('#footer_action_button').addClass('glyphicon-check');
 //     $('#footer_action_button').removeClass('glyphicon-trash');
 //     $('.actionBtn').addClass('btn-success');
 //     $('.actionBtn').removeClass('btn-danger');
 //     $('.actionBtn').addClass('edit');
 //     $('.modal-title').text('Post Edit');
 //     $('.deleteContent').hide();
 //     $('.form-horizontal').show();
 //     $('#fid').val($(this).data('id'));
 //     $('#t').val($(this).data('description'));
 //     $('#myModal').modal('show');
 // });
 // 
 // $('.modal-footer').on('click', '.edit', function() {
 //   $.ajax({
 //     type: 'POST',
 //     url: '/administrator/supplier/editSupplier',
 //     data: {
 //             '_token': $('input[name=_token]').val(),
 //             'id': $("#fid").val(),
 //             'title': $('#t').val()
 //     },
 // success: function(data) {
 //       $('.post' + data.id).replaceWith(" "+
 //       "<tr class='post" + data.id + "'>"+
 //       "<td>" + data.id + "</td>"+
 //       "<td>" + data.description + "</td>"+
 //       "<td>" + data.created_at + "</td>"+
 //       "<td><button class='show-modal btn btn-info btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='fa fa-eye'></span></button> <button class='edit-modal btn btn-warning btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-pencil'></span></button> <button class='delete-modal btn btn-danger btn-sm' data-id='" + data.id + "' data-description='" + data.description + "'><span class='glyphicon glyphicon-trash'></span></button></td>"+
 //       "</tr>");
 //     }
 //   });
 // });
 // 
 // // form Delete function
 // $(document).on('click', '.delete-modal', function() {
 //     $('#footer_action_button').text(" Delete");
 //     $('#footer_action_button').removeClass('glyphicon-check');
 //     $('#footer_action_button').addClass('glyphicon-trash');
 //     $('.actionBtn').removeClass('btn-success');
 //     $('.actionBtn').addClass('btn-danger');
 //     $('.actionBtn').addClass('delete');
 //     $('.modal-title').text('Delete Post');
 //     $('.id').text($(this).data('id'));
 //     $('.deleteContent').show();
 //     $('.form-horizontal').hide();
 //     $('.title').html($(this).data('title'));
 //     $('#myModal').modal('show');
 // });
 // 
 // $('.modal-footer').on('click', '.delete', function(){
 //   $.ajax({
 //     type: 'POST',
 //     url: '/administrator/supplier/deleteSupplier',
 //     data: {
 //       '_token': $('input[name=_token]').val(),
 //       'id': $('.id').text()
 //     },
 //     success: function(data){
 //        $('.post' + $('.id').text()).remove();
 //     }
 //   });
 // });
 // 
 //   // Show function
 //   $(document).on('click', '.show-modal', function() {
 //     $('#show').modal('show');
 //     $('#i').text($(this).data('id'));
 //     $('#ti').text($(this).data('description'));
 //     $('.modal-title').text('Show Post');
 //   });
  </script>
@endsection
