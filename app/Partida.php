<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partida extends Model
{
    protected $table = "partidas";

	protected $primaryKey = 'id';
	protected $fillable = [
        'codigo',
        'nombre'
    ];
    public function solicitudes()
	{
		return $this->belongsToMany('App\Solicitud')->withTimestamps();
    }

}
