<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = "proveedores";

		protected $primaryKey = 'id';

		protected $fillable = [
			'descripcion',
			'direccion'
        ];
        
        public function referencias()
		{
			return $this->belongsToMany('App\Referrencia')->withTimestamps();
		}
}
