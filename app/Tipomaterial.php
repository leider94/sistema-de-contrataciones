<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipomaterial extends Model
{
    protected $table = "tipo_materiales";

		protected $primaryKey = 'id';

		protected $fillable = [
			'descripcion'
        ];
}
