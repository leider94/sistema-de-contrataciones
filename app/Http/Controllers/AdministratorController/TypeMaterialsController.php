<?php

namespace App\Http\Controllers\AdministratorController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tipomaterial;
use Validator;
use Response;
use Illuminate\Support\Facades\Input;
class TypeMaterialsController extends Controller
{
    public function index(){
        $typematerials = Tipomaterial::paginate(20);
        return view('layouts.administrator.typematerials.index',compact('typematerials'));
      }
  
    public function addTypeMaterial(Request $request){
      //dd($request);
      $rules = array(
        'title' => 'required',
      );
    
        $validator = Validator::make ( Input::all(), $rules);
       
        if ($validator->fails())
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));

        else {
          $typematerials = new Tipomaterial;
          $typematerials->descripcion = $request->title;
          $typematerials->save();
          return response()->json($typematerials);
        }
    }
  
      public function editTypeMaterial(request $request){
        $typematerials = Tipomaterial::find ($request->id);
        $typematerials->descripcion = $request->title;
        $typematerials->save();
        return response()->json($typematerials);
      }
  
      public function deleteTypeMaterial(request $request){
        $typematerials = Tipomaterial::find ($request->id)->delete();
        return response()->json();
      }
}
