<?php

namespace App\Http\Controllers\AdministratorController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use Response;   
use App\Proveedor;
use App\Tiporeferencia;
use App\Referencia;
use Illuminate\Support\Facades\Input;
class SupplierController extends Controller
{
    public function index(){
        $proveedor = Proveedor::paginate(20);
        $tipofererencia = Tiporeferencia::pluck('descripcion','id');
        return view('layouts.administrator.suppliers.index',compact('proveedor','tipofererencia'));
      }
    public function addSupplier(Request $request){
      $rules = array(
        'title' => 'required',
        'direction' => 'required'
      );
        $validator = Validator::make ( Input::all(), $rules);

        if ($validator->fails())
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
        else {

          $referencesArray = Input::get('array');

          foreach ($referencesArray as $key => $value) {
           
            $reference = new Referencia;
            $reference->description=$referencesArray[$key].
            $reference->typereferences_id=
            $reference->save();
          }



          

          

          $supplier = new Supplier;
          $supplier->descripcion = $request->title;
          $supplier->direccion = $request->direction;
          $supplier->save();
          return response()->json($names);
        }
    }

      public function editSupplier(request $request){
        $proveedor = Proveedor::find ($request->id);
        $proveedor->descripcion = $request->title;
        $proveedor->save();
        return response()->json($proveedor);
      }

      public function deleteSupplier(request $request){
        $proveedor = Proveedor::find ($request->id)->delete();
        return response()->json();
      }
}
