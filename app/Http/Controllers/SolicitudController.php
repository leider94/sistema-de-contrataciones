<?php

namespace App\Http\Controllers;
use App\Tipocontratacion;
use App\Subtipocontratacion;
use App\Partida;
use App\Solicitud;
use Validator;
use DB;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use PhpOffice\PhpWord\Shared\Converter;
use PhpOffice\PhpWord\Style\Language;
use PhpOffice\PhpWord\Style\ListItem;
use PhpOffice\PhpWord\Style\NumberingLevel;


class SolicitudController extends Controller
{

    public function index()
    {
        //$request = Hiringtype::paginate(20);
        $solicitud=Solicitud::paginate(20);
        $tipocontratacion = Tipocontratacion::pluck('nombre','id');

        return view('layouts.documentos.index',compact('solicitud','tipocontratacion'));
    }
    public function generadorWord(){
      $phpWord = new \PhpOffice\PhpWord\PhpWord();
        // New portrait section
       //$section = $phpWord->addSection(array("paperSize" => "Letter"));








      $PidPageSettings = array(
       "paperSize" => "Letter",
       'headerHeight'=> \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.2),
       'footerHeight'=> \PhpOffice\PhpWord\Shared\Converter::inchToTwip(0),
       //'marginLeft'  => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5),
       //'marginRight' => \PhpOffice\PhpWord\Shared\Converter::inchToTwip(.5),
       //'marginTop'   => 1,
       //'marginBottom'=> 1
      );
      $section = $phpWord->createSection($PidPageSettings);
      
      $header = $section->addHeader();

        $header->addWatermark('./images/escudoBolivia.png',array('width' => 105, 'height' => 73,
        'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(0.2)),
        'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1)),
        'posHorizontal' => 'absolute','posVertical' => 'absolute'));

        $header->addWatermark('./images/fonabosque.png',array('width' => 120, 'height' => 60,
        'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-0.25)),
        'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(4.80)),
        'posHorizontal' => 'absolute','posVertical' => 'absolute'));


        $header->addWatermark('./images/logoMMAYA.png', 
        array('width' => 130, 'height' => 41,
        'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-0.4)),
        'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(10.15)),
        'posHorizontal' => 'absolute','posVertical' => 'absolute'));




        
       
        $header->addWatermark('./images/footer-info.png',
        array('width' => 612, 'height' => 55,
        'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(17)),
        'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1.75)),
        'posHorizontal' => 'absolute','posVertical' => 'absolute'));



        //$estiloTitulo=array('size' => 12, 'Arial' => true,'bold' => true,'bgColor' => 'fbbb10');
        //$section->addText('NOTA INTERNA',$estiloTitulo,array('align'=>'center'));

        $phpWord->addTitleStyle(1, array('size' => 12, 'Arial' => true,'bold' => true), array('align' => 'center' ));
        $section->addTitle('NOTA INTERNA', 1);
        



        
        $esttiloFuente = array("name" => "Arial Narrow",
                        "size" => 11,
                      //  "color" => "8bc34a",
                      //  "italic" => true,
                      //  "bold" => true
                      );
        $esttiloFuente2 = array("name" => "Arial Narrow",
          "size" => 11,
        //  "color" => "8bc34a",
        //  "italic" => true,
          "bold" => true
        );

        

        //$phpWord->addParagraphStyle("P-listStyle", array("spaceAfter"=>0,"lineHeight"=>1.0));
        //$listItemRun = $section->addListItemRun(0,null, "P-listStyle");
        //$listItemRun->addText("List item 1");
        //$listItemRun->addText(" in bold", array("bold"=>true));

        //$estiloTabla = array('borderSize' => 6, 'borderColor' => '999999','align' => 'center');
        $table = $section->addTable(array('align' => 'center'));
        $table->addRow();
        $table->addCell(2000, array('valign'=>'top'))->addText("A",$esttiloFuente2, array('align' => 'center',"spaceAfter"=>0,"lineHeight"=>1.0));
        $table->addCell(2000, array('valign'=>'top'))->addText(":",$esttiloFuente2, array('align' => 'center',"spaceAfter"=>0,"lineHeight"=>1.0));
        $table->addCell(8000, array('valign'=>'top'))->addText("Lic. Soto",$esttiloFuente,array("spaceAfter"=>0,"lineHeight"=>1.0)); 
        $table->addRow();
        $table->addCell(2000, array('valign'=>'center'))->addText("");
        $table->addCell(2000, array('valign'=>'center'))->addText("");
        $table->addCell(8000, array('valign'=>'center'))->addText("Responsable del Proceso de Contratación",$esttiloFuente2); 
        
        $table->addRow();
        $table->addCell(2000, array('valign'=>'top'))->addText("DE",$esttiloFuente2, array('align' => 'center',"spaceAfter"=>0,"lineHeight"=>1.0));
        $table->addCell(2000, array('valign'=>'top'))->addText(":",$esttiloFuente2, array('align' => 'center',"spaceAfter"=>0,"lineHeight"=>1.0));
        $table->addCell(8000, array('valign'=>'top'))->addText("Justo Nicanor",$esttiloFuente,array("spaceAfter"=>0,"lineHeight"=>1.0)); 
        $table->addRow();
        $table->addCell(2000, array('valign'=>'center'))->addText("");
        $table->addCell(2000, array('valign'=>'center'))->addText("");
        $table->addCell(8000, array('valign'=>'center'))->addText("Encargado de",$esttiloFuente2);
       
        $table->addRow();
        $table->addCell(2000, array('valign'=>'top'))->addText("REF.",$esttiloFuente2, array('align' => 'center'));
        $table->addCell(2000, array('valign'=>'top'))->addText(":",$esttiloFuente2, array('align' => 'center'));
        $casoContratacion=$table->addCell(8000, array('valign'=>'top'));
        $textoSeguido=$casoContratacion->addTextRun(array('align' => 'both'));
        $textoSeguido->addText("Solicitud de Autorización de Inicio de Proceso para la Contratación de ",$esttiloFuente);
        $textoSeguido->addText("(OBJETO DE LA ADQUISICIÓN DE BIENES O CONTRATACIÓN DE SERVICIOS)",$esttiloFuente2); 

        $table->addRow();         
        $table->addCell(2000, array('valign'=>'top'))->addText("CITE",$esttiloFuente2, array('align' => 'center'));
        $table->addCell(2000, array('valign'=>'top'))->addText(":",$esttiloFuente2, array('align' => 'center'));
        $table->addCell(8000, array('valign'=>'top'))->addText("DGE-JNC-O247-NOT/19",$esttiloFuente); 

        $table->addRow();
                
        $table->addCell(2000,array('valign'=>'top'))->addText("FECHA",$esttiloFuente2, array('align' => 'center'));
        $table->addCell(2000,array('valign'=>'top'))->addText(":",$esttiloFuente2, array('align' => 'center'));
        $table->addCell(8000,array('valign'=>'top'))->addText("Lunes, 02 de Septiembre de 2019",$esttiloFuente); 

        $table->addRow();
        $cellStyle = array('borderBottomSize' => 6,'borderBottomColor'=>'999999','valign'=>'top'); 
        $table->addCell(2000,$cellStyle)->addText("");
        $table->addCell(2000,$cellStyle)->addText("");
        $table->addCell(8000,$cellStyle)->addText("");

        //Estilos para los párrafos
        $fontStyle = array("name" => "Arial Narrow","size" => 11, );
        $phpWord->addFontStyle('miPropioEstilo', $fontStyle);
        
        $estiloParrafo = array('align'=>'both');
        $phpWord->addParagraphStyle('parrafo',$estiloParrafo);

        //Aqui comienza el párrafo.
        $section->addTextBreak();
        $section->addText("De mi consideración:");

        $textoCorrido=$section->addTextRun('parrafo');
        $textoCorrido->addText("De acuerdo a las actividades programadas en el POA de la presente gestión, .... (Ejemplo: La Coordinación de Planificación
        ______________) ha dispuesto la", 'miPropioEstilo');
        $textoCorrido->addText(" (OBJETO DE LA ADQUISICIÓN DE BIENES O CONTRATACIÓN DE SERVICIOS)");
        $textoCorrido->addText("________, según el siguiente detalle:", 'miPropioEstilo');

        $phpWord->addFontStyle(
          'estiloLista',
          array(
            'name' => 'Arial Narrow',
              'size'    => '10',
              'bold'    => true,
              //'allCaps' => true,
              //'scale'   => 200,
              //'spacing' => 240,
              //'kerning' => 10,
          ));

        $phpWord->addNumberingStyle(
          'multilevel',
          array(
              'type'   => 'multilevel',
              'levels' => array(
                  //array('format' => 'decimal', 'text' => '%1.', 'left' => 360, 'hanging' => 360, 'tabPos' => 360),
                  array('format' => 'lowerLetter', 
                          'text' => '%1)', 
                          'left' => 720,
                       'hanging' => 360,
                        'tabPos' => 720,
                          'font' => 'Arial Rounded MT Bold'),
              ),
          )
      );
    //  $predefinedMultilevel = array('listType' => \PhpOffice\PhpWord\Style\ListItem::TYPE_NUMBER_NESTED);
      // Lists

      $listItemRun = $section->addListItemRun(0,'multilevel', 'parrafo');
      $listItemRun->addText('Antecedentes:','estiloLista');
      $listItemRun->addText('Antecedentes:asddaaaaaasdddddddddddd dddddddddddddddddd dddddddddd  sadasdas sdasdasdasdasdasasdas adaad');



      $section->addListItem('Objetivo:', 0, 'estiloLista', 'multilevel');

      $section->addListItem('Justificación Técnica', 0, 'estiloLista', 'multilevel');
      
      $section->addTextBreak(2);


$section->addText(htmlspecialchars('List with inline formatting.'));
$listItemRun = $section->addListItemRun();
$listItemRun->addText(htmlspecialchars('List item 1'));
$listItemRun->addText(htmlspecialchars(' in bold'), array('bold' => true));
$listItemRun = $section->addListItemRun();
$listItemRun->addText(htmlspecialchars('List item 2'));
$listItemRun->addText(htmlspecialchars(' in italic'), array('italic' => true));
$listItemRun = $section->addListItemRun();
$listItemRun->addText(htmlspecialchars('List item 3'));
$listItemRun->addText(htmlspecialchars(' underlined'), array('underline' => 'dash'));
$section->addTextBreak(2);



        


        $fuente = [
          "name" => "Courier new",
          "size" => 20,
          "color" => "000000",
          "italic" => true,
      ];
      $tipo = [
        'listType' => ListItem::TYPE_ALPHANUM,
      ];
        $seccion = $phpWord->addSection();
$seccion->addListItem("Elemento con profundidad por defecto");
$seccion->addListItem("Elemento con profundidad 1", 1,$fuente,$tipo);
$seccion->addListItem("Elemento con profundidad 1", 1,$fuente,$tipo);
$seccion->addListItem("Elemento con profundidad 1", 1,$fuente,$tipo);
$seccion->addListItem("Elemento con profundidad 1", 1,$fuente,$tipo);
$seccion->addListItem("Elemento con profundidad 3", 3,$fuente,$tipo);
$seccion->addListItem("Elemento con profundidad 3", 3,$fuente,$tipo);

$seccion->addListItem("Elemento con profundidad 1 y con fuente", 1, $fuente);
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_SQUARE_FILLED", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_SQUARE_FILLED,
    ]);
}
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_BULLET_FILLED", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_BULLET_FILLED,
    ]);
}
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_BULLET_EMPTY", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_BULLET_EMPTY,
    ]);
}
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_NUMBER", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_NUMBER,
    ]);
}
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_NUMBER_NESTED", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_NUMBER_NESTED,
    ]);
}
for ($profundidad = 1; $profundidad < 4; $profundidad++) {
    $seccion->addListItem("Elemento con profundidad $profundidad, con fuente y tipo de lista TYPE_ALPHANUM", $profundidad, $fuente, [
        'listType' => ListItem::TYPE_ALPHANUM,
    ]);
}
        










        //$header->addImage('./images/escudoBolivia.png', # sets the path to ./storage/
        //                  array(
        //                      //'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
        //                      //'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_LEFT,
        //                      //'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
        //                      //'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
        //                      //'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
        //               
        //                      'width'=>100, 
        //                      'height'=>70));
        
       // $header->addImage('./images/escudoBolivia.png', array('width' => 100, 'height' => 70));

        //$header->addWatermark('./images/header-logos.png', # sets the path to ./storage/
        //array('width' => 612, 'height' => 111,
        //'marginTop' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1.5)),
        //'marginLeft' => round(\PhpOffice\PhpWord\Shared\Converter::cmToPixel(-1.75)),
        //'posHorizontal' => 'absolute','posVertical' => 'absolute'));


        



        //$styleUNO=array('mariginTop'=>11);
       // $styleDOS=array('positioning' => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE,
       // 'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
       // 'posVertical'    => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
       // 'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
       // 'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_PAGE,
       // 'width'=>115,'height'=>65);


       


        





        //$header->addImage('./images/logoMMAYA.png', # sets the path to ./storage/
        //array('width' => 130, 'height' => 41));//'positioning' => 'absolute',
        //'alignment' => 'right','wrappingStyle'=>'behind'));


        //$table = $header->addTable(array( 'align' => 'center','positioning'=>"absolute",'posHorizontal'=>'page','posVertical'=>'page'));
        //$table->addRow();
        //$table->addCell(4000,array('valign'=>'center'))->addImage("./images/escudoBolivia.png",array('align'=>'left','width'=>105, 'height'=>73));
        //$table->addCell(4000,array('valign'=>'center'))->addImage("./images/fonabosque.png",array('align'=>'center','width'=>120, 'height'=>60));        
        //$table->addCell(4000,array('valign'=>'center'))->addImage("./images/logoMMAYA.png",array('align'=>'right','width'=>130, 'height'=>41));
        
        //for ($c = 1; $c <= 3; $c++) {
        //  $table->addCell(1750)->addText("Row 1, Cell {$c}");
        //}
        //$header->addText('This is my fabulous header!');


        

        //$footer = $section->addFooter();      
        //$table = $footer->addTable(array( 'align' => 'center'));
        //$myfontstyle = array('size' => 9);
        //$table->addRow();
        //$table->addCell(6000,array('align'=>'left','valign'=>'bottom'))->addText("www.fonabosque.gob.bo");
        //$table->addCell(6000,array('valign'=>'bottom'))->addText('Calle Almirante Grau Nro.557,Piso 1,entre Calle ' 
        //                                                                          .'Zoilo Flores y Boquerón, Zona San Pedro '
        //                                                                          .'Telf.:(591-2)2129838-2128772 '
        //                                                                          .'Fax:(591-2)-2128772 '
        //                                                                          .'info@fonabosque.gob.bo',$myfontstyle,array('align'=>'right'));       
        //
        
        
        

        //$header = array('size' => 16, 'bold' => true);
        //
        //// 1. Basic table
        //
        //$rows = 10;
        //$cols = 5;
        //$section->addText('Basic table', $header);
        //
        //$table = $section->addTable(array('positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_ABSOLUTE));
        //for ($r = 1; $r <= 8; $r++) {
        //    $table->addRow();
        //    for ($c = 1; $c <= 5; $c++) {
        //        $table->addCell(1750)->addText("Row {$r}, Cell {$c}");
        //    }
        //}
        //
        //// 2. Advanced table
        //
        //$section->addTextBreak(1);
        //$section->addText('Fancy table', $header);
        //
        //$fancyTableStyleName = 'Fancy Table';
        //$fancyTableStyle = array('borderSize' => 6, 'borderColor' => '006699', 'cellMargin' => 80, 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER, 'cellSpacing' => 50);
        //$fancyTableFirstRowStyle = array('borderBottomSize' => 18, 'borderBottomColor' => '0000FF', 'bgColor' => '66BBFF');
        //$fancyTableCellStyle = array('valign' => 'center');
        //$fancyTableCellBtlrStyle = array('valign' => 'center', 'textDirection' => \PhpOffice\PhpWord\Style\Cell::TEXT_DIR_BTLR);
        //$fancyTableFontStyle = array('bold' => true);
        //$phpWord->addTableStyle($fancyTableStyleName, $fancyTableStyle, $fancyTableFirstRowStyle);
        //$table = $section->addTable($fancyTableStyleName);
        //$table->addRow(900);
        //$table->addCell(2000, $fancyTableCellStyle)->addText('Row 1', $fancyTableFontStyle);
        //$table->addCell(2000, $fancyTableCellStyle)->addText('Row 2', $fancyTableFontStyle);
        //$table->addCell(2000, $fancyTableCellStyle)->addText('Row 3', $fancyTableFontStyle);
        //$table->addCell(2000, $fancyTableCellStyle)->addText('Row 4', $fancyTableFontStyle);
        //$table->addCell(500, $fancyTableCellBtlrStyle)->addText('Row 5', $fancyTableFontStyle);
        //for ($i = 1; $i <= 8; $i++) {
        //    $table->addRow();
        //    $table->addCell(2000)->addText("Cell {$i}");
        //    $table->addCell(2000)->addText("Cell {$i}");
        //    $table->addCell(2000)->addText("Cell {$i}");
        //    $table->addCell(2000)->addText("Cell {$i}");
        //    $text = (0 == $i % 2) ? 'X' : '';
        //    $table->addCell(500)->addText($text);
        //}
        //
        ///*
        // *  3. colspan (gridSpan) and rowspan (vMerge)
        // *  ---------------------
        // *  |     |   B    |    |
        // *  |  A  |--------|  E |
        // *  |     | C |  D |    |
        // *  ---------------------
        // */
        //
        //$section->addPageBreak();
        //$section->addText('Table with colspan and rowspan', $header);
        //
        //$fancyTableStyle = array('borderSize' => 6, 'borderColor' => '999999');
        //$cellRowSpan = array('vMerge' => 'restart', 'valign' => 'center', 'bgColor' => 'FFFF00');
        //$cellRowContinue = array('vMerge' => 'continue');
        //$cellColSpan = array('gridSpan' => 2, 'valign' => 'center');
        //$cellHCentered = array('alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER);
        //$cellVCentered = array('valign' => 'center');
        //
        //$spanTableStyleName = 'Colspan Rowspan';
        //$phpWord->addTableStyle($spanTableStyleName, $fancyTableStyle);
        //$table = $section->addTable($spanTableStyleName);
        //
        //$table->addRow();
        //
        //$cell1 = $table->addCell(2000, $cellRowSpan);
        //$textrun1 = $cell1->addTextRun($cellHCentered);
        //$textrun1->addText('A');
        //$textrun1->addFootnote()->addText('Row span');
        //
        //$cell2 = $table->addCell(4000, $cellColSpan);
        //$textrun2 = $cell2->addTextRun($cellHCentered);
        //$textrun2->addText('B');
        //$textrun2->addFootnote()->addText('Column span');
        //
        //$table->addCell(2000, $cellRowSpan)->addText('E', null, $cellHCentered);
        //
        //$table->addRow();
        //$table->addCell(null, $cellRowContinue);
        //$table->addCell(2000, $cellVCentered)->addText('C', null, $cellHCentered);
        //$table->addCell(2000, $cellVCentered)->addText('D', null, $cellHCentered);
        //$table->addCell(null, $cellRowContinue);
        //
        ///*
        // *  4. colspan (gridSpan) and rowspan (vMerge)
        // *  ---------------------
        // *  |     |   B    |  1 |
        // *  |  A  |        |----|
        // *  |     |        |  2 |
        // *  |     |---|----|----|
        // *  |     | C |  D |  3 |
        // *  ---------------------
        // * @see https://github.com/PHPOffice/PHPWord/issues/806
        // */
        //
        //$section->addPageBreak();
        //$section->addText('Table with colspan and rowspan', $header);
        //
        //$styleTable = array('borderSize' => 6, 'borderColor' => '999999');
        //$phpWord->addTableStyle('Colspan Rowspan', $styleTable);
        //$table = $section->addTable('Colspan Rowspan');
        //
        //$row = $table->addRow();
        //$row->addCell(1000, array('vMerge' => 'restart'))->addText('A');
        //$row->addCell(1000, array('gridSpan' => 2, 'vMerge' => 'restart'))->addText('B');
        //$row->addCell(1000)->addText('1');
        //
        //$row = $table->addRow();
        //$row->addCell(1000, array('vMerge' => 'continue'));
        //$row->addCell(1000, array('vMerge' => 'continue', 'gridSpan' => 2));
        //$row->addCell(1000)->addText('2');
        //
        //$row = $table->addRow();
        //$row->addCell(1000, array('vMerge' => 'continue'));
        //$row->addCell(1000)->addText('C');
        //$row->addCell(1000)->addText('D');
        //$row->addCell(1000)->addText('3');
        //
        //// 5. Nested table
        //
        //$section->addTextBreak(2);
        //$section->addText('Nested table in a centered and 50% width table.', $header);
        //
        //$table = $section->addTable(array('width' => 50 * 50, 'unit' => 'pct', 'alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER));
        //$cell = $table->addRow()->addCell();
        //$cell->addText('This cell contains nested table.');
        //$innerCell = $cell->addTable(array('alignment' => \PhpOffice\PhpWord\SimpleType\JcTable::CENTER))->addRow()->addCell();
        //$innerCell->addText('Inside nested table');
        //
        //// 6. Table with floating position
        //
        //$section->addTextBreak(2);
        //$section->addText('Table with floating positioning.', $header);
        //
        //$table = $section->addTable(array('borderSize' => 6, 'borderColor' => '999999', 'position' => array('vertAnchor' => TablePosition::VANCHOR_TEXT, 'bottomFromText' => Converter::cmToTwip(1))));
        //$cell = $table->addRow()->addCell();
        //$cell->addText('This is a single cell.');
        $phpWord->getSettings()->setThemeFontLang(new Language(Language::ES_ES));
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $objWriter->save('Appdividend.docx');
        return response()->download(public_path('Appdividend.docx'));
  }

    function añadirSolicitud(Request $request){
      $rules = array(
        'tipo1' => 'required',
        'tipo2' => 'required',
        'objeto' => 'required',
        'objetivo' => 'required',
        'antecedentes' => 'required',
        'justificacion' => 'required'
      );
        $validator = Validator::make ( Input::all(), $rules);

        if ($validator->fails())
        return Response::json(array('errors'=> $validator->getMessageBag()->toarray()));
        else {

          $checks_partidas=json_decode($request->get('ckecks'),true);
          $checks_lenght=count($checks_partidas);
          $user = Auth::user();
          $datoSolicitud["objeto"]=$request->get('objeto');
          $datoSolicitud["objetivo"]=$request->get('objetivo');
          $datoSolicitud["antecedente"]=$request->get('antecedentes');
          $datoSolicitud["justificacion_tecnica"]=$request->get('justificacion');
          $datoSolicitud["usuario_id"]=$user->id;
          $datoSolicitud["tipocontratacion_id"]=$request->get('tipo1');
          if ($request->get('tipo1')!=1) {
            $datoSolicitud["subtipocontratacion_id"]=null;
          }else{
            $datoSolicitud["subtipocontratacion_id"]=$request->get('tipo2');
          }
          $solicitud=Solicitud::create($datoSolicitud);
          for ($i=0; $i < $checks_lenght ; $i++) {   
            $partida=Partida::findOrFail($checks_partidas[$i]);
            $solicitud->partidas()->attach($partida);
          }
          
          
          
//
         // $referencesArray = Input::get('array');
//
         // foreach ($referencesArray as $key => $value) {
         //  
         //   $reference = new Referencia;
         //   $reference->description=$referencesArray[$key].
         //   $reference->typereferences_id=
         //   $reference->save();
         // }
//
//
//
         // 
//
         // 
//
         // $supplier = new Supplier;
         // $supplier->descripcion = $request->title;
         // $supplier->direccion = $request->direction;
         // $supplier->save();
          return response()->json($solicitud);
        }
    }

    function partidas(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      $checks = json_decode($request->get('arraycheck'),true);
      if($query != '')
      {
       $data = DB::table('partidas')
         ->orWhereNotIn('id', $checks)  
         ->where('codigo', 'like', '%'.$query.'%')
         ->orWhere('nombre', 'like', '%'.$query.'%')
         ->orderBy('id', 'desc')
         ->get();
         
      }
      else
      {
       $data = DB::table('partidas')
         ->orWhereNotIn('id', $checks)
         ->orderBy('id', 'desc')
         ->get();
      }
      $arr_length = count($checks);
      if ($arr_length>0) {
        $partidasChecks = DB::table('partidas')
        ->whereIn('id', $checks)
        ->orderBy('id', 'desc')
        ->get();
        $total_row_check = $partidasChecks->count();
        if ($total_row_check>0) {
            foreach($partidasChecks as $row)
            {
             $output .= '
             <tr>
              <td>'.$row->codigo.'</td>
              <td>'.$row->nombre.'</td>
              <td><input id="nacional" name="nacional" type="checkbox" class="flat-red" value="'.$row->id.'" checked></td>
             </tr>
             ';
            }
        }
          
      }
      $total_row = $data->count();
      if($total_row > 0)
      {
       foreach($data as $row)
       {
        $output .= '
        <tr>
         <td>'.$row->codigo.'</td>
         <td>'.$row->nombre.'</td>
         <td><input id="nacional" name="nacional" type="checkbox" class="flat-red" value="'.$row->id.'"></td>
        </tr>
        ';
       }
      }
      else
      {
        if ($arr_length>0){
          $output .= '
          <tr>
           <td align="center" colspan="5">Partida no encontrada</td>
          </tr>
          ';

        }else{
          $output = '
          <tr>
           <td align="center" colspan="5">Partida no encontrada</td>
          </tr>
          ';
        }
       
      }
      $data = array(
       'table_data'  => $output,
       'total_data'  => $total_row,
       'checks' => $request->get('arraycheck')      
    );

      return json_encode($data);
     }
    }

    public function porTipo($id){
        return Subtipocontratacion::where('tipocontratacion_id','=',$id)->get();
    }
}


