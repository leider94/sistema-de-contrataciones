<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtipocontratacion extends Model
{
    protected $table = "subtipo_contratacion";

	protected $primaryKey = 'id';
	protected $fillable = [
		'nombre',
		'tipocontratacion_id'
    ];

    public function tipoContratacion()
	{
		return $this->belongsTo('App\Tipocontratacion');
	}
	
	public function solicitud()
	{
		return $this->hasMany('App\Solicitud');
    }
}
