<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tipocontratacion extends Model
{
    protected $table = "tipo_contratacion";

	protected $primaryKey = 'id';
	protected $fillable = [
        'nombre'
    ];
    public function subTipoContatacion()
	{
		return $this->hasMany('App\Subtipocontratacion');
    }

    public function solicitud()
	{
		return $this->hasMany('App\Solicitud');
    }

}
