<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Solicitud extends Model
{
    protected $table = "solicitud";

	protected $primaryKey = 'id';
	protected $fillable = [
        'objeto',
        'objetivo',
        'antecedente',
        'justificacion_tecnica',
        'usuario_id',
        'tipocontratacion_id',
        'subtipocontratacion_id'
    ];
    public function usuarios()
	{
		return $this->belongsTo('App\users')->withTimestamps();
    }
    public function partidas()
	{
		return $this->belongsToMany('App\Partida')->withTimestamps();
    }
    public function subTipoContratacion()
	{
		return $this->belongsTo('App\Subtipocontratacion');
    }
    public function tipoContratacion()
	{
		return $this->belongsTo('App\Tipocontratacion');
	}

}
